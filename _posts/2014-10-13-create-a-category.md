---
category: Product
path: '/v1/companies/:company_id/categories'
title: 'Create a category'
type: 'POST'

layout: default
---

This method allows to create a category.

### Request

* **`:company_id`** is the id the selected company.
* The headers must include a **valid authentication token**.

```{
       "name": "Food",
       "description": null
}```

### Response

Sends back a response code with the created category.

```Status: 201 CREATED```

```{
       "id": 1,
       "name": "Food",
       "description": null
}```

For errors responses, see the [response status codes documentation](#/response-status-codes).