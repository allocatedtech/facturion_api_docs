---
category: Product
path: '/v1/companies/:company_id/categories/:id'
title: 'Get a category'
type: 'GET'

layout: default
---

This method allows fetching an existing category.

### Request

* **`:company_id`** is the id the selected company.
* **`:id`** is the id the selected category.
* The headers must include a **valid authentication token**.
* **The body is omitted**.

### Response

Sends back a response code with the requested category.

```Status: 200 OK```

```{
       "id": 1,
       "name": "Food",
       "description": null
}```

For errors responses, see the [response status codes documentation](#/response-status-codes).