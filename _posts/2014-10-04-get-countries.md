---
category: Country
path: '/v1/countries'
title: 'Get countries'
type: 'GET'

layout: default
---

This method allows fetching a list of ISO3166-1 countries, containing their country code and name.

### Request

* The headers must include a **valid authentication token**.

### Response

Sends back a collection of companies.

```Status: 200 OK```
```[
       ["WF", "Wallis and Futuna"],
       ["JP", "Japan"],
       ["JM", "Jamaica"],
       ["JO", "Jordan"],
       ["WS", "Samoa"],
       ["JE", "Jersey"],
       ["GW", "Guinea-Bissau"],
       ["GU", "Guam"],
       ["GT", "Guatemala"],
       ["GS", "South Georgia and the South Sandwich Islands"],
       ["GR", "Greece"],
       ["GQ", "Equatorial Guinea"],
       ["GP", "Guadeloupe"],
       ["GY", "Guyana"],
       ["GG", "Guernsey"],
       ["GF", "French Guiana"],
       ["GE", "Georgia"],
       ["GD", "Grenada"],
       ["GB", "United Kingdom"],
       ...
]```

For errors responses, see the [response status codes documentation](#/response-status-codes).