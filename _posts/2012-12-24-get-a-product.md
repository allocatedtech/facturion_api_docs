---
category: Product
path: '/v1/companies/:company_id/products/:id'
title: 'Get a product'
type: 'GET'

layout: default
---

This method allows fetching an existing product.

### Request

* **`:company_id`** is the id the selected company.
* **`:id`** is the id the selected product.
* The headers must include a **valid authentication token**.
* **The body is omitted**.

### Response

Sends back a response code with the requested product.

```Status: 200 OK```

```{
    "id": 3,
    "name": "Bread",
    "price": "1.55",
    "vat": "6.00",
    "category": 1,
    "description": ""
}```

For errors responses, see the [response status codes documentation](#/response-status-codes).