---
category: Product
path: '/v1/companies/:company_id/products'
title: 'Create a product'
type: 'POST'

layout: default
---

This method allows to create a product.

### Request

* **`:company_id`** is the id the selected company.
* The headers must include a **valid authentication token**.

```{
    "name": "Bread",
    "price": "1.55",
    "vat": "6.00",
    "category": 1,
    "description": ""
}```

### Response

Sends back a response code with the created product.

```Status: 201 CREATED```

```{
    "id": 3,
    "name": "Bread",
    "price": "1.55",
    "vat": "6.00",
    "category": 1,
    "description": ""
}```

For errors responses, see the [response status codes documentation](#/response-status-codes).