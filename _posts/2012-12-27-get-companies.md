---
category: Company
path: '/v1/companies'
title: 'Get companies'
type: 'GET'

layout: default
---

This method allows users to fetch all companies.

### Request

* The headers must include a **valid authentication token**.

### Response

Sends back a collection of companies.

```Status: 200 OK```
```[
    {
        id: 1,
        name: "Allocated Tech",
        active: true,
        description: "",
        email: "info@allocated.technology",
        address: {
            line_1: "Vunderke 9",
            line_2: "",
            line_3: "", 
            postal_code: "5404PW",
            city: "Uden", 
            phone_nr: "", 
            province: "",
            country: "NL"
        }, 
        company_info: {
            chamber_of_commerce_identifier: 1234,
            tax_identifier: "NL1234",
            account_nr: "464175607",
            website: "http://facturion.nl",
            telephone: "0627052196"
        },
        added_at: "2014-07-26T13:36:30.006Z",
        user: [
            {
            user: 1,
            level: 0,
            added_at: "2014-07-26T13:36:51.832Z"
            }
        ]
    }, ....
]
```

For errors responses, see the [response status codes documentation](#/response-status-codes).