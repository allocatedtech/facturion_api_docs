---
category: Product
path: '/v1/companies/:company_id/products/:id'
title: 'Update a product'
type: 'PUT'

layout: default
---

This method allows updating an existing product.

### Request

* **`:company_id`** is the id the selected company.
* **`:id`** is the id the product to update.
* The headers must include a **valid authentication token**.

```{
       "id": 3,
       "name": "Bread",
       "price": "1.55",
       "vat": "6.00",
       "category": 1,
       "description": ""
}```

### Response

Sends back a response code with current data.

```Status: 200 OK```

```{
          "id": 3,
          "name": "Bread",
          "price": "1.55",
          "vat": "6.00",
          "category": 1,
          "description": ""
}```

For errors responses, see the [response status codes documentation](#/response-status-codes).