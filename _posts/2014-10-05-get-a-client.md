---
category: Client
path: '/v1/companies/:companyId/clients/:clientId'
title: 'Get a client'
type: 'GET'

layout: default
---

This method retrieves a client from a specific company.

### Request

* The headers must include a **valid authentication token**.
* **`:companyId`** is required.
* **`:clientId`** is required.


### Response

Sends back the requested client.

```Status: 200 OK```
```{
       "id": 5,
       "name": "MARS",
       "is_company": true,
       "description": "",
       "email": null,
       "address": {
           "line_1": "Taylorweg 5",
           "line_2": "",
           "line_3": "",
           "postal_code": "5466 AE",
           "city": "Veghel",
           "phone_nr": "+31 413 383333",
           "province": "",
           "country": "NL"
       }
}```

For errors responses, see the [response status codes documentation](#/response-status-codes).