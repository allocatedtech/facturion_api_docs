---
category: Product
path: '/v1/companies/:company_id/categories/:id'
title: 'Update a category'
type: 'PUT'

layout: default
---

This method allows updating an existing category.

### Request

* **`:company_id`** is the id the selected company.
* **`:id`** is the id the category to update.
* The **`products`** property is **read only**
* The headers must include a **valid authentication token**.

```{
       "id": 1,
       "name": "Food",
       "description": null
}```

### Response

Sends back a response code with current data.

```Status: 200 OK```

```{
       "id": 1,
       "name": "Food",
       "description": null,
       "products": [{
           "id": 3,
           "name": "Bread",
           "price": "1.50",
           "vat": "6.00",
           "category": 1,
           "description": ""
       }, {
           "id": 5,
           "name": "Milk",
           "price": "0.90",
           "vat": "6.00",
           "category": 1,
           "description": ""
       }]
}```

For errors responses, see the [response status codes documentation](#/response-status-codes).