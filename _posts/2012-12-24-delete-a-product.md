---
category: Product
path: '/v1/companies/:company_id/products/:id'
title: 'Delete a product'
type: 'DELETE'

layout: default
---

This method allows deleting an existing product.

### Request

* **`:company_id`** is the id the selected company.
* **`:id`** is the id the product to delete.
* The headers must include a **valid authentication token**.
* **The body is omitted**.

### Response

Sends back a response code.

```Status: 204 No content```


For errors responses, see the [response status codes documentation](#/response-status-codes).