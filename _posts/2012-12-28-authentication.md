---
category: Authentication
path: '/v1/auth/token-based'
title: 'Authenticate'
type: 'POST'

layout: default
---

This method allows users to authenticate them self.

### Request

```
{
    username: "user@email-provider.com"
    password: "password"
}
```

### Response

Sends back a collection of things.

```Authorization: <TOKEN>```
```{
    token: "6z652d282249521b43696e327e63bebcb38"
}```